package com.onecoder.feudbot;


/**
 * A valid move on the board, a word that is valid and its score
 * @author andreasblomqvist
 *
 */
public class Move {

	int startingPos;
	int stopPos;
	String word;
	int score;
		
	
	public Move(int startingPos, int stopPos, String word, int score) {
		super();
		this.startingPos = startingPos;
		this.stopPos = stopPos;
		this.word = word;
		this.score = score;
	}
	
	
	public int getStartingPos() {
		return startingPos;
	}
	public void setStartingPos(int startingPos) {
		this.startingPos = startingPos;
	}
	public int getStopPos() {
		return stopPos;
	}
	public void setStopPos(int stopPos) {
		this.stopPos = stopPos;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	
	
}
