package com.onecoder.feudbot;
import java.util.List;

/**
 * Representation of a scabble board
 * 
 * @author andreasblomqvist
 * 
 */
public class Board {

	final int NONE = 0;
	final static int TL = 1;
	final static int TW = 2;
	final static int DL = 3;
	final static int DW = 4;
	
	final static int UP = 0;
	final static int DOWN = 1;
	final static int LEFT = 2;
	final static int RIGHT = 3;
	final static int BLANK = 4;

	Tile[][] board = new Tile[15][15];
	Tile[][] move = new Tile[15][15];
	int[][] multipler = new int[15][15];

	int rows = 15;
	int cols = 15;

	/*
	public Board(char[][] board, int rows, int cols) {
		super();
		this.board = board;
		this.rows = rows;
		this.cols = cols;
	}
	*/

	public Board() {
		// TODO Auto-generated constructor stub
	}


	public void clearBoard() {

		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				board[col][row] = null;
				multipler[col][row]=NONE;
			}
		}

	}

	public void clearMove() {

		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				move[col][row] = null;
			}
		}

	}

	public void putBrick(int col, int row, Tile tile) {
		board[col][row] = tile;
	}

	public void putWord(int col, int row, int direction, Tile[] word) {

		for (int c = 0; c < word.length;) {
			char letter = word[c].letter;

			switch (direction) {
			case LEFT:
				
				break;
			case RIGHT:

				break;
			case DOWN:

				break;
			case UP:

				break;

			default:
				break;
			}

		}

	}

	/**
	 * Checks if a position is next to a letter brick
	 * 
	 * @param col
	 * @param row
	 * @return boolean
	 */
	public boolean boundary(int col, int row) {

		// check that value of position is not a blank, left
		if (col > 0 && !isEmptyPos(col - 1,row)) {
			return true;
		}

		// check that value of position is not a blank, right
		if (col < this.cols - 1 && !isEmptyPos(col + 1,row)) {
			return true;
		}

		// check that value of position is not a blank, up
		if (row > 0 && !isEmptyPos(col,row - 1)) {
			return true;
		}

		// check that value of position is not a blank, down
		if (row < this.rows - 1 && !isEmptyPos(col,row + 1)) {
			return true;
		}

		return false;
	}

	private boolean isEmptyPos(int col, int row ){

		return board[col][row] == null;
	}

	/**
	 * 
	 * @param word
	 * @param i
	 * @param j
	 * @param m
	 * @param n
	 * @return
	 */
	private int[] put_word(String word, int i, int j, int m, int n) {
		int l = word.length();
		m = 1;

		for (int o = i; o >= m; o++) {
			for (int p = j; p >= n; p++) {
				if (board[o][p] == null)

					// ??
					// board[o][p]=word.substring(m);
					m = m + 1;
				if (m > l) {

					int[] ret = new int[2];
					ret[0] = o;
					ret[1] = p;

					return ret;
				}
			}
		}

		// for o from i to m
		// for p from j to n
		// if(empty(board, o, p)
		// board(o,p) = word(m)
		// m = m +1
		// if(m>l)
		// return (o,p)
		// endif
		// next
		// next

		return null;
	}

	/*
	 * private List<String> check_board(char[] letters, int i, int j) { //
	 * permutations: array of words String[] permutations = null;
	 * 
	 * permutations = permute(letters);
	 * 
	 * // start = (i,j)
	 * 
	 * int[] start = null; start[0] = i; start[1] = j;
	 * 
	 * // for each word in permutations
	 * 
	 * for (String s : permutations) {
	 * 
	 * int[][] new_board = board;
	 * 
	 * String word; int[] end; int score; List<String> result = null;
	 */

	/*
	 * //end is tupel (int array). check for what?? if(end = put_word(new_board,
	 * word, start[0],start[1], board.length, j))
	 * score=test_word(new_board,start, end)
	 * 
	 * 
	 * if(score>0) add((word, score), result)
	 * 
	 * new_board=board; if(end = put_word(new_board, word, start, (1, j))
	 * score=test_word(new_board,start, end) if(score>0) add((word, score),
	 * result)
	 * 
	 * new_board=board; if(end = put_word(new_board, word, start, (i,
	 * board.length)) score=test_word(new_board,start, end) if(score>0)
	 * add((word, score), result)
	 * 
	 * new_board=board; if(end = put_word(new_board, word, start, (i, 1))
	 * score=test_word(new_board,start, end) if(score>0) add((word, score),
	 * result)
	 */

	/*
	 * return result; } return null; }
	 */

}
