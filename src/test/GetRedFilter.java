package test;

import java.awt.image.RGBImageFilter;


// This filter removes all but the red values in an image
class GetRedFilter extends RGBImageFilter {
    public GetRedFilter() {
        // When this is set to true, the filter will work with images
        // whose pixels are indices into a color table (IndexColorModel).
        // In such a case, the color values in the color table are filtered.
        canFilterIndexColorModel = true;
    }

    // This method is called for every pixel in the image
    public int filterRGB(int x, int y, int rgb) {
        if (x == -1) {
            // The pixel value is from the image's color table rather than the image itself
        }
        // Return only the red component
        return rgb & 0xffff0000;
        			  
    }
}