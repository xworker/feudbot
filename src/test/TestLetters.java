package test;

import static org.junit.Assert.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.junit.Test;

public class TestLetters {

	
	@Test
	public void testGray(){
		
		String fileName="OYTW.PNG";
		
		URL u = ClassLoader.getSystemResource("LETTERS/"+fileName);
		BufferedImage colorImage = null;
		try {
			colorImage = ImageIO.read(u);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedImage grayImage =grayImage(colorImage);
		
		//makeRed(grayImage, fileName);
		
		try {
			// // Save as PNG
			File file = new File(fileName);
			ImageIO.write(grayImage, "PNG", file);
			
		} catch (IOException e) {
		}
	}
	
	
	@Test
	public void test() {

		String[] letterArr = { "A", "B", "C", "D", "E", "F", "G", "H", "I",
				"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
				"V", "W", "X", "Y", "Z", "�", "�", "�" };
		
		HashMap<String, BufferedImage> letterImages=null;
		HashMap<String, BufferedImage> letterImagesSub =null;
		
		try {
		 letterImages = loadAllLetters(letterArr,
				false);
		 System.out.println("loaded images "+letterImages.size());

		letterImagesSub = loadAllLetters(
					letterArr, true);
		 System.out.println("loaded sub images "+letterImagesSub.size());
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Entry<String, BufferedImage> b : letterImagesSub.entrySet()) {
			try {
				
				
				String k=b.getKey().substring(0,1);
				System.out.println("comparing "+b.getKey()+".PNG with "+k+".PNG");
				
				compareImages(b.getValue(), letterImages.get(k));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private BufferedImage makeRed(Image image, String fileName){
		
		 // Create the filter
		ImageFilter filter = new GetRedFilter();
		  FilteredImageSource filteredSrc = new FilteredImageSource(
		  image.getSource(), filter);
		  
		  // Create the filtered image 
		 image =  Toolkit.getDefaultToolkit().createImage(filteredSrc);
		  
		  // print the new image try { // // Save as PNG 
		  File file = new File(fileName); 
		  try {
		 ImageIO.write(imageToBufferedImage(image),
			  "png", file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		  
		
	}
	
	private static BufferedImage imageToBufferedImage(Image image) {

		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
				image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bufferedImage.createGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();

		return bufferedImage;

	}
	
	
	private static BufferedImage grayImage(BufferedImage colorImage){
		
		int width = colorImage.getWidth();
		int height = colorImage.getHeight();
		BufferedImage image = new BufferedImage(width, height,
			    BufferedImage.TYPE_BYTE_BINARY);
			Graphics g = image.getGraphics();
			g.drawImage(colorImage, 0, 0, null);
			g.dispose();
			return image;
	}
	
	

	
	
	private HashMap<String, BufferedImage> loadAllLetters(String[] letters, boolean sub) throws IOException{
		
		
		HashMap<String, BufferedImage> allLetters = new HashMap<String, BufferedImage>();

		for(String s:letters){
			
			URL u =null;	
			
		
			if(sub){
				 u = ClassLoader.getSystemResource(s+"_sub.PNG");
				 
				
			}else{
				 u = ClassLoader.getSystemResource("LETTERS/"+s+".PNG");
				 
			}
			
			
			if(u==null){
			//	System.out.println("Failed to load image for "+s);
				continue;
			}
			
			
			
				if(sub){
					allLetters.put(s+"_sub",
							ImageIO.read(u));
				}else{
					allLetters.put(s,
							ImageIO.read(u));
				}
			
			
		}
		
		
		
		return allLetters;
		
	}

	private boolean compareImages(BufferedImage image1, BufferedImage image2)
			throws IOException {

		Raster r1 = image1.getData();
		Raster r2 = image2.getData();
		boolean ret = false; // Stores result.

		// Check image sizes and number of bands. If there are different
		// then no need to compare images as they are definitely not equal.

		if (r1.getNumBands() != r2.getNumBands()) {
			ret = false;
		} else {
			// #Bands and image bounds match so compare each sample in turn.

			ret = true;
			int diff = 0;

			search: for (int i = 0; i < r1.getNumBands(); ++i) {
				for (int x = 0; x < r1.getWidth(); ++x) {
					for (int y = 0; y < r1.getHeight(); ++y) {
						if (r1.getSample(x, y, i) != r2.getSample(x, y, i)) {
							// At least one sample differs so result is false;
							diff++;
							// Use labeled break to terminate all loops.

						}
					}
				}
			}
			System.out.println("diff is " + diff);
			if (diff > 25) {
				ret = false;
			}
		}

		return ret;
	}

}
