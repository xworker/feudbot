package test;

public class FundGroup {

	
	Long fundGroupId;

	
	
	public FundGroup() {
		super();
	}

	public Long getFundGroupId() {
		return fundGroupId;
	}

	public void setFundGroupId(Long fundGroupId) {
		this.fundGroupId = fundGroupId;
	}

	@Override
	public String toString() {
		return "FundGroup [fundGroupId=" + fundGroupId + "]";
	}
	
	
}
