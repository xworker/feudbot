package test;

import java.util.Arrays;
import java.util.List;

public class Fund {

	Long fundId;
	FundGroup[] groupList;
	
	public Fund() {
		// TODO Auto-generated constructor stub
	}

	
	
	
	public Long getFundId() {
		return fundId;
	}




	public void setFundId(Long fundId) {
		this.fundId = fundId;
	}




	public FundGroup[] getGroupList() {
		return groupList;
	}

	public void setGroupList(FundGroup[] fgList) {
		this.groupList = fgList;
	}




	@Override
	public String toString() {
		return "Fund [fundId=" + fundId + ", groupList="
				+ Arrays.toString(groupList) + "]";
	}





	

}
